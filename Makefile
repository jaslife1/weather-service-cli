build:
	docker build -t weather_service_cli .

run:
	docker run \
		-e MICRO_REGISTRY=mdns \
		weather_service_cli