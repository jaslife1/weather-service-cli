package main

import (
	"context"
	"log"
	"time"

	"github.com/jaslife1/uniuri"
	micro "github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	pb "gitlab.com/lazybasterds/weather-service/proto/weather"
)

func main() {
	srv := micro.NewService(

		micro.Name("weather-service-cli"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	// Create a new client
	c := pb.NewWeatherServiceClient("weather-service", client.DefaultClient)
	// Get current time information
	{
		id := uniuri.New()

		start := time.Now()

		log.Println("RequestID: ", id)
		ctx := metadata.NewContext(context.Background(), map[string]string{
			"requestID": id,
		})
		duration := time.Now().Add(1 * time.Second)
		ctx, cancel := context.WithDeadline(ctx, duration)
		defer cancel()
		resp, err := c.GetWeather(ctx, &pb.Locale{Locale: "Berlin,DE"})

		if err != nil {
			log.Panicf("Error: Did not get any Weather information - %v", err)
		}

		log.Printf("Weather Info: %v", resp.Weather)

		elapsed := time.Since(start)
		log.Printf("Elapsed time: %s", elapsed)
	}
}
